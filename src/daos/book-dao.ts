// DAO (Data Access Object)
// A class that is responsible for persisting an entity 
// DAO should support the CRUD operations, Create, Read, Update, Delete

import { Book } from "../entities";

export interface BookDAO{
    //create
    createBook(book:Book): Promise<Book>;// return a promise that will eventually be a book
    //read
    getAllBooks(): Promise<Book[]>;
    getBookByID(bookId:number): Promise<Book>
    //update
    updateBook(book:Book):Promise<Book>;
    //delete
    deleteBookByID(bookId:number):Promise<boolean>;
}