import { Book } from "../entities";
import { MissingResourceError } from "../errors";
import { BookDAO } from "./book-dao";
//import { readFile, writeFile } from "fs/promises";
const {readFile, writeFile} = require( "fs").promises;

export class BookDaoTextFile implements BookDAO{
    async createBook(book: Book): Promise<Book> {
        const fileData = await readFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // make the JSON text and turn it into an object
        book.bookId = (Math.round(Math.random()*1000)); // random number id for the book
        books.push(book);// add our book to the array
        await writeFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt', JSON.stringify(books))
        return book;
    }
    async getAllBooks(): Promise<Book[]> {
        const fileData = await readFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // make the JSON text and turn it into an object
        return books;
    }
   async getBookByID(bookId: number): Promise<Book> {
        const fileData = await readFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // make the JSON text and turn it into an object


        for(const book of books)
        {
            if(book.bookId === bookId)
            {
                return book;
            }
        }
        throw new MissingResourceError(`The book with id ${bookId} could not  be located`);
    }
    async updateBook(book: Book): Promise<Book> {
        const fileData = await readFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // make the JSON text and turn it into an object
      
       for(let i = 0; i < books.length;i++)
       {
           if(books[i].bookId === book.bookId)
           {
               books[i] = book;
           }
       }

        await writeFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt', JSON.stringify(books))
        return book;
    }
    // async functions must always return a promise
    async deleteBookByID(bookId: number): Promise<boolean> {
        const fileData = await readFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // make the JSON text and turn it into an object
      
       for(let i = 0; i < books.length;i++)
       {
            if(books[i].bookId === bookId)
            {
                books.splice(i)// remove that book
                await writeFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt', JSON.stringify(books))
                return true;
            }
       }

        //await writeFile('C:\\Users\\Jesse\\Desktop\\Revature\\Day 6\\LibraryAPI\\books.txt', JSON.stringify(books))
        return false;
    }

    
}