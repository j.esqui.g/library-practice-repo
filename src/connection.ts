import { Client } from "pg";
//import os from 'os' in mac

export const client = new Client({
    user:'postgres', // make come back to this
    password: process.env.DBPASSWORD, // should never store passwords in code
    database: 'librarydb',
    port:5432,
    host: '34.145.179.198'
})

client.connect()

// for windows DBPASSWORD is a creation enviromental variable that we created for windows
//