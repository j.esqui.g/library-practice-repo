// An entity is a class that store information that will ultimately be persisted somewhere
// this clas usually contains minimal logic
// Should always have one field in them that is a unique identifier , ID

export class Book{
    
    constructor(
        public bookId:number,
        public title:string,
        public author: string,
        public isAvailable:boolean,
        public quality: number,
        public returnDate: number // typically dates are stored in unix epcoh time 
        // seconds from midnight January 1970
        )
    {

    }
}
