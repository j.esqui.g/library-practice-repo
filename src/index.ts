import express from 'express';
import { BookServiceImpl } from './services/book-service-impl';
import BookService from './services/book-services';
import { Book } from './entities';
import { MissingResourceError } from './errors';

const app = express();

app.use(express.json()); // Middleware

// Our application routes should use services we create to do the heavy lifting
// try to minimize the amount of logic in your routes that is NOT related directly to HTTP requests and responses

const bookService:BookService = new BookServiceImpl();

app.get("/books", async (req,res)=>{
    try {
        const books: Book[]= await bookService.retrieveAllBooks();
        res.send(books);
    } catch (error) {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/books/:id", async(req,res)=>{
    // const bookId = Number(req.params.id);
    // const book:Book = await bookService.retrieveBookById(bookId);
    // res.send(book);

    try {
        const bookId = Number(req.params.id);
        const book:Book = await bookService.retrieveBookById(bookId);
        res.send(book);
    } catch (error) {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
})

app.post("/books",async (req,res)=>{
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
})

app.patch("/books/:id/checkout", async(req, res)=>{
    const bookid = Number(req.params.id);
    const book = await bookService.checkoutBookById(bookid);
    res.send(book);
})

app.listen(3000,()=> {console.log("Application started")});