import { Book } from "../entities";

// Your service interface should have all the methods that your RESTful web service
// will find helpful
// 1. Methods that perform CRUD operations
// 2. Business Logic operations
export default interface BookService{

//withdraw deposit functions, takes in account as parameter for project

    registerBook(book:Book): Promise<Book>;

    retrieveAllBooks():Promise<Book[]>;

    retrieveBookById(bookId:number):Promise<Book>;

    checkoutBookById(bookId:number):Promise<Book>;

    checkinBookById(bookId:number):Promise<Book>;

    searchByTitlte(title:string): Promise<Book[]>;

    modifyBook(book:Book):Promise<Book>;

    removeBookById(bookId:number):Promise<boolean>;
}