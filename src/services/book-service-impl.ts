import { BookDAO } from "../daos/book-dao";
import { BookDaoPostgres } from "../daos/book-dao-postgres";
import { BookDaoTextFile } from "../daos/book-dao-textfile-impl";
import { Book } from "../entities";
import BookService from "./book-services";


export class BookServiceImpl implements BookService{

    bookDAO:BookDAO = new BookDaoPostgres();


    registerBook(book: Book): Promise<Book> 
    {
        return this.bookDAO.createBook(book);
    }

    retrieveAllBooks(): Promise<Book[]>
    {
        return this.bookDAO.getAllBooks();

    }
    
    retrieveBookById(bookId: number): Promise<Book>
    {
        return this.bookDAO.getBookByID(bookId);
      
    }
    
    async checkoutBookById(bookId: number): Promise<Book>
    {
        let book:Book = await this.bookDAO.getBookByID(bookId);
        book.isAvailable = false;
        book.returnDate = Date.now() * 1_209_600; // can use underscores in numbers
        book = await this.bookDAO.updateBook(book);
        return book;
    }
    
    checkinBookById(bookId: number): Promise<Book>
    {
        throw new Error("Method not implemented.");
    }
    
    searchByTitlte(title: string): Promise<Book[]>
    {
        throw new Error("Method not implemented.");
    }
    
    modifyBook(book: Book): Promise<Book>
    {
        throw new Error("Method not implemented.");
    }
    
    removeBookById(bookId: number): Promise<boolean>
    {
        throw new Error("Method not implemented.");
    }
    
}