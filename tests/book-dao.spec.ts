import { BookDAO } from "../src/daos/book-dao";
import { BookDaoPostgres } from "../src/daos/book-dao-postgres";
import { BookDaoTextFile } from "../src/daos/book-dao-textfile-impl";
import { Book } from "../src/entities";
import { client } from "../src/connection";

const bookDAO: BookDAO = new BookDaoPostgres();

// Any entity object that has NOT been saved somewhere should have the ID of 0
// this is a standard software convention
const testBook:Book = new Book(0,'The Hobbit', 'Tollkien', true, 1, 0);

test("Create a book", async()=>{
    const result:Book = await bookDAO.createBook(testBook);
    expect(result.bookId).not.toBe(0);
})

//An integreation test required that two or more functions you wrote pass
test("Get book by ID", async()=>{
    let book:Book = new Book(0,'Dracula', "Bram Stoker", true, 1, 0);
    book = await bookDAO.createBook(book);

    let retrievedBook:Book = await bookDAO.getBookByID(book.bookId)// avoid hard coded id values in a  // difficult to maintain

    expect(retrievedBook.title).toBe(book.title);

})

test("Get all books", async()=>{
    let book1:Book = new Book(0,'Sapiens','Yuval',true,0,0);
    let book2:Book = new Book(0,'1984', 'George Orwell',true,1,1);
    let book3:Book = new Book(0,'Paradox of choice', 'Barry Schwartz', true,0,0);

    await bookDAO.createBook(book1);
    await bookDAO.createBook(book2);
    await bookDAO.createBook(book3);

    const books:Book[] = await bookDAO.getAllBooks();

    expect(books.length).toBeGreaterThanOrEqual(3);
})

test("update book", async()=>{
    let book:Book = new Book(0,'We have always lived in the castle','Shirley Jackson',true,1,0)
    book = await bookDAO.createBook(book);

    // to update an object we just edit it and then pass it into a method
    book.quality = 4;

    book = await bookDAO.updateBook(book);


    //const updateBook = await bookDAO.getBookByID(book.bookId);

    expect(book.quality).toBe(4);

})

test("delete book by id",async()=>{
    let book:Book = new Book(0,'Frankestein','Mary Shelley',true,1,0);
    book = await bookDAO.createBook(book);// create book

    const result:boolean =  await bookDAO.deleteBookByID(book.bookId); // wait for this to finish with await

    expect(result).toBeTruthy()
})


afterAll(async()=>{
    client.end();
})
// try to avoid methods in your program that return void
// methods that do not return data are very difficult to test
// for the most part, you want your methods to take in objects